class App {
  String name = '';
  String sector = '';
  String category = '';
  String developer = '';
  int year = 0;
  App(String aName, String aSector, String aCategory, String aDeveloper,
      int aYear) {
     name = aName;
     sector = aSector;
     category = aCategory;
     developer = aDeveloper;
     year = aYear;
  }
  info() {
    print(name.toUpperCase());
    print('Sector: $sector');
    print('Category: $category');
    print('Developed by: $developer');
    print('Won in: $year');
  }
}

void main() {
  App app2012 =
      App('FNB Banking app', 'consumer sector', 'best iOS app', 'fnb', 2012);
  App app2013 = App(
      'SnapScan', 'consumer sector', 'best HTML5 app', 'Gerrit Greef', 2013);
  App app2014 = App('Live Inspect', 'consumer sector', 'best Android app',
      'Lightstone', 2014);
  App app2015 = App('WumDrop', 'enterprise sector', 'best enterprise app',
      'Simon Hartley', 2015);
  App app2016 = App('Domestly', 'consumer sector', 'best consumer app',
      'Berno Potgieter', 2016);
  App app2017 = App('Shyft', 'finance sector', 'best financial solution',
      'Breth Patrontasch', 2017);
  App app2018 = App('Khula', 'consumer', 'best agriculture solution',
      'Karidas Tshintsholo', 2018);
  App app2019 = App('Naked Insurance', 'finance sector',
      'best financial solution', 'Sumarie Greybe', 2019);
  App app2020 = App('EasyEquity', 'investment sector', 'best consumer solution',
      'Charles Savage', 2020);
  App app2021 = App('Ambani', 'education sector', 'best educational solution',
      'mukundi Lambani', 2021);
  app2012.info();
  app2013.info();
  app2014.info();
  app2015.info();
  app2016.info();
  app2017.info();
  app2018.info();
  app2019.info();
  app2020.info();
  app2021.info();
}
